const {readFileS, writeFileSync, readFile, writeFile} = require('fs')
console.log('Inicia')
readFile('./Content/first.txt', 'utf8', (err, result) => {//se não especificar 'utf8' apresenta um Buffer
    if (err){
        console.log(err);
        return
    }
    const first = result;

    readFile('./Content/second.txt', 'utf8', (err, result) => {//se não especificar 'utf8' apresenta um Buffer
        if (err){
            console.log(err);
            return
        }
        const second = result;
        writeFile('./Content/result-async.txt', `The third file is: ${first}, ${second}`,
        (err, result) => {
            if(err){
                console.log('Tarefa terminada')
                return 
            }
            
        });
    });

})

console.log('Começando próxima tarefa')