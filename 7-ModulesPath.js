const path = require('path');//Acessa os módulos
console.log(path.sep);//Apresenta o separador

const filePath = path.join('/Content/', 'subfolder', 'teste.txt');//"Busca" o endereço de um arquivo 
console.log(filePath);

const base = path.basename(filePath);
console.log(base);

const absolute = path.resolve(__dirname, 'Content','subfolder','test.txt');//Endereço absoluto de um arquivo 
console.log(absolute);