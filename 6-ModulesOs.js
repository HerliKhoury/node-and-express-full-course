const os = require('os')

//Informações do usuário atual
const user = os.userInfo()
console.log(user)

//Metodo que retorna o uptime em segundos
console.log(`The System Uptime is ${os.uptime()} seconds`);

const currentOS = {//Extrai informações do sistema como...
    name: os.type(),//Tipo
    release: os.release(),//versão
    totalMem: os.totalmem(),//memória total
    freeMem: os.freemem()//mémoria livre
}

console.log(currentOS);//Apresenta dados extraidos